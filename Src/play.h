#ifndef PLAY_H
#define PLAY_H

#include "stm32f4xx_hal.h"

#define PLAY_BUFF_SIZE 1000

uint8_t *get_buffer();
void play_is_actual();

void play_tick();

void play_callback();

uint8_t play_get_free_buffer();

#endif