#include "play.h"


uint8_t play_buffer[PLAY_BUFF_SIZE];

extern DAC_HandleTypeDef hdac;

uint8_t actual = 0;

static uint16_t _play_cnt = 0;


typedef enum
{
	PLAY_CH_1,
	CH_1_TO_CH_2,
	PLAY_CH_2,
	CH_2_TO_CH_1,
}Play_state;

Play_state state = PLAY_CH_1;

void play(uint8_t *buffer_1, uint8_t *buffer_2, uint16_t size)
{
	HAL_DAC_Start(&hdac, DAC_CHANNEL_1);

}


uint8_t *get_buffer()
{
	return play_buffer;
}

void play_is_actual()
{
	actual = _play_cnt + 1;
}

uint8_t play_get_free_buffer()
{
	switch(state)
	{
	case PLAY_CH_1:
		return 1;
		break;
	case CH_1_TO_CH_2:
		return 0;
		break;
	case PLAY_CH_2:
		return 0;
	case CH_2_TO_CH_1:
		return 1;
	}
}


void play_tick()
{
	
    fflush(stdout);
	switch(state)
	{
	case PLAY_CH_1:
		if(_play_cnt > sizeof(play_buffer) / 2)
		{
			state = CH_1_TO_CH_2;
		}
		break;
	case CH_1_TO_CH_2:
		play_callback();
		state = PLAY_CH_2;
		break;
	case PLAY_CH_2:
		if(_play_cnt >= sizeof(play_buffer))
		{
			_play_cnt = 0;
			state = CH_2_TO_CH_1;
		}
		break;
	case CH_2_TO_CH_1:
		play_callback();
		state = PLAY_CH_1;
		break;
	}

	if(actual != _play_cnt)
	{

		HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1, DAC_ALIGN_8B_R, play_buffer[_play_cnt++]);
	}
}